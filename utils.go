package main

import "fmt"

func printMatrix(b [][]bool) {
	for col := range b {
		for _, val := range b[col] {
			fmt.Printf("%t, ", val)
		}
		fmt.Println("")
	}
	fmt.Println("")
}

func (t *tetramino) printConstruction() {
	printMatrix(t.shape.construction)
	fmt.Printf("w: %v, h: %v\n", t.w, t.h)
}
