package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type gridPoint struct {
	x, y  int
	color sdl.Color
	fill  bool
}

type glass struct {
	filling [20][28]gridPoint
}

func newGlass() (*glass, error) {
	var matrix [20][28]gridPoint
	for i := 0; i < 28; i++ {
		matrix[0][i].fill = true
		matrix[0][i].color = sdl.Color{R: 200, G: 200, B: 200, A: 20}
		matrix[19][i].fill = true
		matrix[19][i].color = sdl.Color{R: 200, G: 200, B: 200, A: 20}
	}
	for i := 1; i < 19; i++ {
		matrix[i][0].fill = true
		matrix[i][0].color = sdl.Color{R: 200, G: 200, B: 200, A: 20}
		matrix[i][27].fill = true
		matrix[i][27].color = sdl.Color{R: 200, G: 200, B: 200, A: 20}
	}
	return &glass{filling: matrix}, nil
}

func (gl *glass) paint(r *sdl.Renderer) error {
	for i := 0; i < 20; i++ {
		for j := 0; j < 28; j++ {
			if gl.filling[i][j].fill {
				r.SetDrawColor(gl.filling[i][j].color.R,
					gl.filling[i][j].color.G,
					gl.filling[i][j].color.B,
					gl.filling[i][j].color.A)
				rect := sdl.Rect{X: (int32)(i * 20),
					Y: (int32)(j * 20),
					W: 20,
					H: 20}
				if err := r.FillRect(&rect); err != nil {
					return fmt.Errorf("Could not fill rect: %v", err)
				}
				if err := r.DrawRect(&rect); err != nil {
					return fmt.Errorf("Could not draw rect: %v", err)
				}
			}
		}
	}
	return nil
}

func (gl *glass) refrash(t *tetramino) error {
	for col := 0; col < t.w; col++ {
		for row := 0; row < t.h; row++ {
			if t.construction[col][row] {
				gl.filling[t.x/20+col][t.y/20+row].fill = true
				gl.filling[t.x/20+col][t.y/20+row].color = t.color
			}
		}
	}
	return nil
}
