package main

import (
	"fmt"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

type sciene struct {
	glass *sdl.Rect
	tetr  *tetramino
	gl    *glass
	speed int
	score int
}

func newSciene(r *sdl.Renderer) (*sciene, error) {
	rect := sdl.Rect{X: 0, Y: 0, W: 400, H: 560}
	tetr, err := newTetramino()
	if err != nil {
		return nil, fmt.Errorf("Could not create tetramino: %v", err)
	}
	glass, err := newGlass()
	if err != nil {
		return nil, fmt.Errorf("Could not create glass: %v", err)
	}
	return &sciene{glass: &rect, tetr: tetr, gl: glass, speed: 1, score: 0}, nil
}

func (s *sciene) paint(r *sdl.Renderer) error {
	if err := r.SetDrawColor(255, 255, 255, 255); err != nil {
		return fmt.Errorf("Could not set draw color: %v", err)
	}
	r.Clear()

	if err := r.SetDrawColor(100, 100, 100, 100); err != nil {
		return fmt.Errorf("Could not set draw color: %v", err)
	}

	if err := r.FillRect(s.glass); err != nil {
		return fmt.Errorf("Could not fill rect: %v", err)
	}

	if err := r.DrawRect(s.glass); err != nil {
		return fmt.Errorf("Could not draw rect: %v", err)
	}

	if err := s.tetr.paint(r, s.gl); err != nil {
		return fmt.Errorf("Could not draw tetramino: %v", err)
	}

	if err := s.gl.paint(r); err != nil {
		return fmt.Errorf("Could not draw glass: %v", err)
	}

	r.Present()

	return nil
}

func (s *sciene) run(event <-chan sdl.Event, r *sdl.Renderer) chan error {
	errc := make(chan error)

	tick := time.Tick((time.Duration)(s.speed) * 20 * time.Millisecond)
	go func() {
		defer close(errc)
		for {
			select {
			case e := <-event:
				if done := s.handleEvent(e); done {
					return
				}
			case <-tick:
				if err := s.paint(r); err != nil {
					errc <- err
				}
			}
		}
	}()

	return errc

}

func (s *sciene) handleEvent(e sdl.Event) bool {
	switch te := e.(type) {
	case *sdl.QuitEvent:
		return true
	//case *sdl.MouseButtonEvent:
	//	return false
	case *sdl.KeyboardEvent:
		if te.Type == sdl.KEYUP {
			switch te.Keysym.Sym {
			case sdl.K_RIGHT:
				s.tetr.move("right")
			case sdl.K_LEFT:
				s.tetr.move("left")
			case sdl.K_SPACE:
				s.tetr.rotate(s.gl)
			}
		}
		//fmt.Printf("%d, %d\n", te.Keysym.Sym, sdl.K_LEFT)

		return false
	default:
		//fmt.Printf("%T\n", e)
		return false
	}
}
