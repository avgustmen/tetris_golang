package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

func init() {
	rand.Seed(time.Now().Unix())
}

const (
	startPositionX = 120
	startPositionY = 100
)

type shape struct {
	construction [][]bool
	color        sdl.Color
	w, h         int
}

func (s *shape) makeShapeBlankArray() [][]bool {
	shapeBlankArray := make([][]bool, s.w)
	for item := range shapeBlankArray {
		shapeBlankArray[item] = make([]bool, s.h)
	}
	return shapeBlankArray
}

func (s *shape) makeRotateShapeBlankArray() [][]bool {
	shapeBlankArray := make([][]bool, s.h)
	for item := range shapeBlankArray {
		shapeBlankArray[item] = make([]bool, s.w)
	}
	return shapeBlankArray
}

type shapes []shape

type tetramino struct {
	*shape
	x, y      int
	collision map[string]bool
}

func takeShape() *shape {
	shapes := shapes{
		shape{
			construction: [][]bool{
				{false, true},
				{true, true},
				{false, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     3,
			h:     2,
		},
		shape{
			construction: [][]bool{
				{false, true},
				{false, true},
				{true, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     3,
			h:     2,
		},
		shape{
			construction: [][]bool{
				{true, false},
				{true, false},
				{true, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     3,
			h:     2,
		},
		shape{
			construction: [][]bool{
				{true, true},
				{true, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     2,
			h:     2,
		},
		shape{
			construction: [][]bool{
				{false, true, true},
				{true, true, false},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     2,
			h:     3,
		},
		shape{
			construction: [][]bool{
				{true, true, false},
				{false, true, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     2,
			h:     3,
		},
		shape{
			construction: [][]bool{
				{true, true, true, true},
			},
			color: sdl.Color{R: 0, G: 255, B: 0, A: 100},
			w:     1,
			h:     4,
		},
	}
	shape := shapes[rand.Intn(len(shapes))]
	//shape := shapes[6]
	return &shape
}

func newTetramino() (*tetramino, error) {
	shape := takeShape()
	collision := newCollision()
	return &tetramino{shape: shape, x: startPositionX, y: startPositionY, collision: collision}, nil
}

func newCollision() map[string]bool {
	collision := map[string]bool{
		"l":  false,
		"r":  false,
		"b":  false,
		"rl": false,
		"rr": false,
		"rb": false,
	}
	return collision
}

func (t *tetramino) paint(r *sdl.Renderer, gl *glass) error {
	t.y++
	t.touch(gl)

	if t.collision["b"] {
		fmt.Println("Тут")
		//t.y--
		if err := gl.refrash(t); err != nil {
			return fmt.Errorf("Could not refrash glass: %v", err)
		}
		fmt.Println("И теперь тут")
		t.shape = takeShape()
		t.x, t.y = startPositionX, startPositionY
		t.collision = newCollision()
	}

	for col := 0; col < t.w; col++ {
		for row := 0; row < t.h; row++ {
			if t.construction[col][row] {
				rect := sdl.Rect{X: (int32)(t.x + col*20),
					Y: (int32)(t.y + row*20),
					W: 20,
					H: 20}
				r.SetDrawColor(t.color.R, t.color.G, t.color.B, t.color.A)
				r.FillRect(&rect)
				r.DrawRect(&rect)
			}
		}
	}

	return nil
}

/*
func (t *tetramino) isCollision(side string) bool {
	switch side {
	case "l":
		if (int)(t.x) <= 20 {
			return true
		}

	case "r":

		if (int)(t.x)+t.w*20 >= 380 {
			return true
		}
	}
	return false
}
*/

func (t *tetramino) rotate(gl *glass) error {
	rotateShape := t.makeRotateShapeBlankArray()
	for countCol, valueCol := range t.construction {
		for countRow, valueRow := range valueCol {
			rotateShape[countRow][t.w-countCol-1] = valueRow
		}
	}

	tTemp, err := newTetramino()
	if err != nil {
		return fmt.Errorf("Could not create tetramino: %v", err)
	}
	tTemp.x, tTemp.y = t.x, t.y
	tTemp.h, tTemp.w = t.w, t.h
	tTemp.construction = rotateShape

	tTemp.touch(gl)

	if !tTemp.collision["rl"] && !tTemp.collision["rr"] && !tTemp.collision["rb"] {
		t.construction = tTemp.construction
		t.w, t.h = tTemp.w, tTemp.h
	}
	return nil
}

func (t *tetramino) move(dir string) {
	if dir == "right" {
		if !t.collision["r"] {
			t.x += 20
		}
	}
	if dir == "left" {
		if !t.collision["l"] {
			t.x -= 20
		}
	}
}

func (t *tetramino) touch(gl *glass) {
	t.collision = newCollision()
	for col := 0; col < t.w; col++ {
		indexCol := calculateIndex(col, t.x)
		for row := 0; row < t.h; row++ {
			indexRow := calculateIndex(row, t.y)
			//fmt.Printf("col: %d, row: %d, indexCol: %d, indexRow: %d\n", col, row, indexCol, indexRow)
			if t.construction[col][row] {
				if indexCol > 18 {
					t.collision["rr"] = true
					t.collision["r"] = true
				} else {
					if gl.filling[indexCol][indexRow].fill {
						t.collision["rl"] = true
					}
					if gl.filling[indexCol-1][indexRow].fill {
						t.collision["l"] = true
					}
					if gl.filling[indexCol][indexRow].fill {
						t.collision["rr"] = true
					}
					if gl.filling[indexCol+1][indexRow].fill {
						t.collision["r"] = true
					}
					if gl.filling[indexCol][indexRow].fill {
						t.collision["rb"] = true
					}
					if gl.filling[indexCol][indexRow+1].fill {
						t.collision["b"] = true
					}
				}
			}
		}
	}
}

func calculateIndex(index int, position int) int {
	return (index*20 + position) / 20
}
