package main

import (
	"fmt"
	"os"
	"runtime"
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

func main() {
	if err := run(); err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(2)
	}
}

func run() error {

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		return fmt.Errorf("Could not init sdl: %v", err)
	}
	defer sdl.Quit()

	if err := ttf.Init(); err != nil {
		return fmt.Errorf("Could not init ttf: %v", err)
	}

	window, r, err := sdl.CreateWindowAndRenderer(800, 600, sdl.WINDOW_SHOWN)
	if err != nil {
		return fmt.Errorf("Could not create window or render: %v", err)
	}
	defer window.Destroy()
	defer r.Destroy()

	if err := drawStartPage(r); err != nil {
		return fmt.Errorf("%v", err)
	}
	defer window.Destroy()
	defer r.Destroy()

	time.Sleep(1 * time.Second)

	s, err := newSciene(r)
	if err != nil {
		return fmt.Errorf("Could not create sciene: %v", err)
	}

	events := make(chan sdl.Event)
	errc := s.run(events, r)

	runtime.LockOSThread()

	for {
		select {
		case events <- sdl.WaitEvent():
		case err := <-errc:
			return err
		}
	}
}

func drawStartPage(r *sdl.Renderer) error {
	r.Clear()

	f, err := ttf.OpenFont("res/fonts/Flappy.ttf", 20)
	if err != nil {
		return fmt.Errorf("Could not load font: %v", err)
	}
	defer f.Close()

	var textColor = sdl.Color{R: 100, G: 100, B: 100, A: 22}
	s, err := f.RenderUTF8Solid("Test tetrise game", textColor)
	if err != nil {
		return fmt.Errorf("Could not get surface: %v", err)
	}
	defer s.Free()

	t, err := r.CreateTextureFromSurface(s)
	if err != nil {
		return fmt.Errorf("Could not render texture: %v", err)
	}

	if err = r.Copy(t, nil, nil); err != nil {
		return fmt.Errorf("Could not copy texture: %v", err)
	}
	r.Present()

	return nil

}
